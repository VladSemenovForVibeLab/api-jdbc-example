# Учет продуктов

Данное приложение разработано на **Spring Boot** версии 3.1.5 и использует следующие зависимости: **spring-boot-starter-data-jdbc**, **spring-boot-starter-web**, **spring-boot-devtools**, **mariaDB**, **lombok**, **maven**.

Приложение позволяет создавать, просматривать, изменять и удалять продукты в базе данных.

## Установка

1. Клонируйте репозиторий на ваш компьютер.
2. Установите и настройте базу данных MariaDB.
3. Откройте проект в вашей любимой IDE.
4. Запустите приложение.

## Использование API

API предоставляет следующие функции:

### 1. Создание продукта

URL: `/insertProduct`
Метод: POST
Тело запроса: JSON объект с данными продукта

### 2. Получение списка продуктов

URL: `/products`
Метод: GET

### 3. Изменение информации о продукте

URL: `/updateProduct/{id}`
Метод: PUT
Параметр: id - идентификатор продукта
Тело запроса: JSON объект с обновленными данными продукта

### 4. Удаление продукта

URL: `/deleteProduct/{id}`
Метод: DELETE
Параметр: id - идентификатор продукта

## Документация Postman

Документация Postman доступна в папке **docs**, в ней вы найдете коллекцию запросов для тестирования API приложения.

## Сущность

```java
package com.sfv.springbootjdbctemplateexample.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "product_table")
public class Product {
    private Integer id;
    private String name;
    private Double price;
    private Integer quantity;
}
```

Сущность **Product** представляет продукт, который содержит следующие поля:
- `id` - идентификатор продукта;
- `name` - название продукта;
- `price` - цена продукта;
- `quantity` - количество продукта.

Каждый продукт хранится в таблице **product_table** в базе данных.