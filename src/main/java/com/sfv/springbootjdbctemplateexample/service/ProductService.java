package com.sfv.springbootjdbctemplateexample.service;

import com.sfv.springbootjdbctemplateexample.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> getProducts();

    void createProduct(Product product);

    void updateProduct(Product product, Integer id);

    void deleteProduct(Integer id);
}
