package com.sfv.springbootjdbctemplateexample.controller;

import com.sfv.springbootjdbctemplateexample.entity.Product;
import com.sfv.springbootjdbctemplateexample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductService productService;
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts(){
        return new ResponseEntity<>(productService.getProducts(), HttpStatus.OK);
    }
    @PostMapping("/insertProduct")
    public ResponseEntity<Void> insert(@RequestBody Product product){
        productService.createProduct(product);
        return ResponseEntity.ok().build();
    }
    @PutMapping("/updateProduct/{id}")
    public ResponseEntity<Void> update(@RequestBody Product product,
                                       @PathVariable Integer id){
        productService.updateProduct(product,id);
        return ResponseEntity.ok().build();
    }
    @DeleteMapping("/deleteProduct/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id){
        productService.deleteProduct(id);
        return ResponseEntity.ok().build();
    }
}
